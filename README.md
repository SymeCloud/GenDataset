# LLM training dataset auto generator

## Dependencies

```bash
# install llama.cpp with CUDA
CMAKE_ARGS="-DLLAMA_BLAS=ON" FORCE_CMAKE=1 pip install llama_cpp_python

pip install -r requirements.txt
```
## Configuration

We use [Llama2-7B-GGUF](https://huggingface.co/SymeCloud/Llama2-7b-Chat-GGUF) as our model. And we put it in the **/models** directory.

The generation may take a long time, depends on the dataset size.
Here're some advices:

- Use CUDA, at least RTX 3050 (8G).
- 7B model is good and fast enough.
- Even you got 1 element/second speed, it still takes long time to finish. Because the result may not pass the verification and have to be regenerated looply.

## Getting started

```bash
python gen.py
```

## Result

The result will be saved as **dataset.[random uuid].json** in the working directory.

Here's an example:

```json
[
 {
    "input": "Write one question or request sentence containing one or more of the following words: east, sex, flight, dance, music, song\n Unterscheidung between a \"singer\" and a \"performer\"?.  Write your answer in complete sentences, using at least two of these words.\n1. Who is the famous singer that has just released a new album?\n2. Have you seen the latest music video by.",
    "output": "3. What was the song that played during the flight?\n4. I love watching dancers perform to live music.\n5. Can you name some of your favorite songs from your favorite artist?\n6. Who is the lead singer of the band that is performing at the festival tonight?."
 },
 
 ......
]
```

## License

GPLv3+

## Credit

This project was inspired by [Open-Source Implementation of WizardLM to turn documents into Q:A pairs for LLM fine-tuning](https://github.com/h2oai/h2o-wizardlm.git).
