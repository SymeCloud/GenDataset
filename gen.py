"""-*-  indent-tabs-mode:nil; coding: utf-8 -*-.

Copyright (C) 2023
    SymeCloud Limited
This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.
If not, see <http://www.gnu.org/licenses/>.

"""

from typing import List
import pandas as pd
import numpy as np
from enum import Enum
import time
from datasets import Dataset, DatasetDict
import llama_cpp
from tqdm.auto import tqdm

class Mutation(Enum):
    FRESH_START = 0
    ADD_CONSTRAINTS = 1
    DEEPEN = 2
    CONCRETIZE = 3
    INCREASE_REASONING = 4
    COMPLICATE = 5
    SWITCH_TOPIC = 6


class GenDataset:
    def __init__(
            self,
            llm_pipeline = None,
            seed_data: List[str] = None,
            num_rows: int = 10,
            min_len_bytes: int = 512,
            max_len_bytes: int = 1024,
            verbose: bool = False,
    ):
        """
        Open-Source Implementation of https://arxiv.org/abs/2304.12244

        :param llm_pipeline: Pipeline that takes a HF dataset containing one string column and returns a list of strings
        :param seed_data: Optional data to create Q:A pairs from, list of strings containing prompts
        :param num_rows: Number of desired Q:A pairs
        :param min_len_bytes: Lower limit for prompt length in bytes
        :param max_len_bytes: Upper limit for prompt length in bytes
        :param verbose: Whether to enable verbose printing.
        """
        self.llm_pipeline = llm_pipeline
        self.num_rows = num_rows
        self.verbose = verbose
        self.seed_text_list = []
        self.seed_data = seed_data
        self.prompts = []
        self.final_prompts = []
        self.final_answers = []
        self.min_len_bytes = min_len_bytes
        self.max_len_bytes = max_len_bytes
        self.prompt_templates = dict()
        self.prompt_templates['base'] = ""
        with open("english-nouns.txt") as f:
            self.nouns = f.readlines()
        seed = None
        np.random.seed(seed)
        self.prompt_templates[Mutation.FRESH_START] = \
            self.prompt_templates['base'] + \
"""Write one question or request sentence containing one or more of the following words: <PROMPT>"""

        self.prompt_templates[Mutation.COMPLICATE] = \
            self.prompt_templates['base'] + \
"""Rewrite #Given Prompt# to make it slightly more complicated, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

        self.prompt_templates[Mutation.ADD_CONSTRAINTS] = \
            self.prompt_templates['base'] + \
"""Add a few more constraints or requirements to #Given Prompt#, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

        self.prompt_templates[Mutation.DEEPEN] = \
            self.prompt_templates['base'] + \
"""Slightly increase the depth and breadth of #Given Prompt#, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

        self.prompt_templates[Mutation.CONCRETIZE] = \
            self.prompt_templates['base'] + \
"""Make #Given Prompt# slightly more concrete, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

        self.prompt_templates[Mutation.INCREASE_REASONING] = \
            self.prompt_templates['base'] + \
"""If #Given Prompt# can be solved with just a few simple thinking processes, rewrite it to explicitly request multi-step reasoning, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

        self.prompt_templates[Mutation.SWITCH_TOPIC] = \
            self.prompt_templates['base'] + \
"""Rewrite #Given Prompt# by switching the topic, keeping the domain and difficulty level similar, and create #New Prompt#.

#Given Prompt#:
<PROMPT>
"""

    def run(self):
        self.create_seed_prompts()
        self.create_prompts()
        self.create_answers()
        list_qa = []
        for i in range(len(self.final_prompts)):
            list_qa.append(
                {
                    'input': self.final_prompts[i],
                    'output': self.final_answers[i]
                }
            )
        import json
        import uuid
        with open("dataset.%s.json" % str(uuid.uuid4())[:4], "wt") as f:
            f.write(json.dumps(list_qa, indent=2))
        print('done')

    def create_seed_prompts(self):
        """
        Turn self.seed_data into a list of strings of text self.source_text_list
        Each text string can represent as little as a word, or as much as document.
        Just has to be representative of some concept or body of text.

        :return: None
        """

        import os
        if isinstance(self.seed_data, str) and os.path.exists(self.seed_data):
            data = pd.DataFrame(self.seed_data)
            if data.shape[1] > 1:
                print("Warning: picking only column 0")
            self.seed_text_list = data.iloc[:, 0].values.tolist()
            assert self.seed_text_list, "data import failed, got empty list"
        else:
            assert isinstance(self.seed_text_list, list)
            if self.seed_data:
                self.seed_text_list = self.seed_data
            else:
                for i in range(self.num_rows * 10):
                    n = np.random.choice([1, 2, 3, 4])
                    self.seed_text_list.append(
                       self.prompt_templates[Mutation.FRESH_START].replace(
                           "<PROMPT>",
                           ", ".join([np.random.choice(self.nouns).strip() for _ in range(n)]))
                    )

    def create_prompts(self):
        print("Creating %d prompts." % self.num_rows)
        assert self.seed_text_list, "must have seed text list"
        t0 = time.time()
        self.prompts.clear()
        for i in range(self.num_rows):
            new_prompt = np.random.choice(self.seed_text_list)
            self.prompts.append(new_prompt)
        i = 0
        while self.mutate(i):
            print("Iteration: %d" % i)
            i += 1
        t1 = time.time()
        print("Done creating %d prompts in %.4f seconds." % (len(self.final_prompts), t1 - t0))
        print(self.final_prompts)

    def create_answers(self):
        print("Creating answers for %d prompts." % len(self.final_prompts))
        t0 = time.time()
        ds = self.convert_list_to_dataset(self.final_prompts)
        self.final_answers = self.llm_pipeline(ds['train'])
        t1 = time.time()
        print("Done creating answers for %d prompts in %.4f seconds." % (ds['train'].num_rows, t1 - t0))

    def convert_list_to_dataset(self, text_list):
        df = pd.DataFrame({'text': text_list})
        ds = DatasetDict()
        ds['train'] = Dataset.from_pandas(df)
        return ds

    def mutate(self, iteration):
        assert len(self.prompts) == self.num_rows
        list_prompts = []
        mutations = []
        for i in range(self.num_rows):
            if iteration == 0 or "Write one question or request containing" in self.prompts[i]:
                mutation = Mutation.FRESH_START
                mutations.append(mutation)
            else:
                mutation = np.random.choice(Mutation)
                mutations.append(mutation)
                if mutation == Mutation.FRESH_START:
                    self.prompts[i] = np.random.choice(self.seed_text_list)
            before = self.prompts[i]
            prompt = self.prompt_templates[mutation].replace("<PROMPT>", before) if iteration else before
            list_prompts.append(prompt)

        ds = self.convert_list_to_dataset(list_prompts)
        assert ds['train'].num_rows == len(list_prompts) == self.num_rows == len(self.prompts)
        t0 = time.time()
        after = self.llm_pipeline(ds['train'])
        head = ds['train'].data['text'][i].as_py()
        assert len(after) == self.num_rows
        t1 = time.time()
        print("Llama took %.4f seconds" % (t1 - t0))

        for i in range(len(after)):
            after[i] = after[i].split("Prompt#:")[-1].strip()
            for pp in ['New Prompt:\n', 'New Prompt: ']:
                if after[i][:len(pp)] == pp:
                    after[i] = after[i][len(pp):]
            after[i] = after[i].replace("As an AI assistant, I", "I")
            after[i] = after[i].replace("As an AI language model, I", "I")
            after[i] = after[i].replace("As an AI assistant, you", "You")
            after[i] = after[i].replace("As an AI language model, you", "You")
            after[i] = after[i].replace("As an AI assistant, what", "What")
            after[i] = after[i].replace("As an AI language model, what", "What")
            after[i] = after[i].strip()
            use_new_prompt, why = self.change_approved(self.prompts[i], after[i])
            if self.verbose:
                print("===========================")
                print("Old Prompt: %s" % self.prompts[i])
                print("Mutation: %s" % mutations[i].name)
                print("New Prompt: %s" % after[i])
                print("===========================")

            if use_new_prompt:
                if self.max_len_bytes >= len(after[i]) >= self.min_len_bytes:
                    self.final_prompts.append(head + after[i])
                    print("Prompt was accepted, now have %d good prompts." % len(self.final_prompts))
                    self.prompts[i] = np.random.choice(self.seed_text_list)
                    print("Creating new prompt.")
                else:
                    self.prompts[i] = after[i]
                    print("Prompt was successfully modified.")
            else:
                print("Mutation rejected, will try again. Reason: %s" % why)
            print("", flush=True)
        return len(self.final_prompts) < self.num_rows

    def change_approved(self, before, after):
        if before == after:
            return False, "same"
        if after.count('\n') > after.count(" ") * 2:
            return False, "too many lines"
        if after.count('\n') == after.count("- ") > 10:
            return False, "too many items"
        if self.prompt_templates['base'] and self.prompt_templates['base'] in after:
            return False, "prompt leaked 1"
        if "#New Prompt#" in after:
            return False, "prompt leaked 2"
        if "new prompt" in after.lower():
            return False, "prompt leaked 3"
        if "how can i assist" in after.lower():
            return False, "AI"
        if "as an ai" in after.lower():
            return False, "AI"
        if "ai assistant" in after.lower():
            return False, "AI"
        if "i'm sorry" in after.lower() and "sorry" not in before.lower() and len(after) < 400:
            return False, "sorry"
            
        check = self.llm_pipeline(self.convert_list_to_dataset(["Is this a complete sentence? just answer yes or no without any explanation: %r" % after])['train'])
        if "No." in check:
            return False, "Wrong sentence"
        return True, "ok"

class LlamaPipeline:
    def __init__(self, model, max_tokens=64, ngl=30):
        self.llama = llama_cpp.Llama(model_path=model, n_gpu_layers=ngl)
        self.max_tokens = max_tokens

    def __call__(self, dataset):
        """
        Passes dataset to LLM and returns the responses.
        :param dataset:  Hugging Face dataset containing a 'text' column with prompts.
        :return: list of strings with responses.
        """
        ret = [];
        for i,s in enumerate(tqdm(dataset.data['text'])):
            out = self.llama.create_completion(s.as_py(), max_tokens=self.max_tokens)
            # remove input in case pipeline is using completion/plain prompt
            response = out['choices'][0]['text']
            response = response.replace(dataset[i]['text'], '').strip()
            ret.append(response + '.')
            print(ret)
        return ret
    
if __name__ == "__main__":
    llm_pipeline = LlamaPipeline(
        "/models/llama-2-7b-chat.gguf.q4_0.bin"
    )    

    gen_data = GenDataset(
        llm_pipeline=llm_pipeline,
        seed_data=None,
        num_rows=10,
        min_len_bytes=64,
        max_len_bytes=1024,
        verbose=False,
    )
    gen_data.run()
